package com.hrd.hw01.repository.provider;

import com.hrd.hw01.model.Book;
import org.apache.ibatis.jdbc.SQL;


public class BookProvider {

    //select all books
    public String select() {
        return new SQL() {{
            SELECT("*");
            FROM("tb_books");
        }}.toString();
    }

    //insert a book record to tb_books
    public String insert() {
        return new SQL() {{
            INSERT_INTO("tb_books");
            VALUES("title", "#{title}");
            VALUES("author", "#{author}");
            VALUES("description", "#{description}");
            VALUES("category_id", "#{category.id}");
        }}.toString();
    }

    //select book by id
    public String selectBookById(int id) {
        return new SQL() {{
            SELECT("*");
            FROM("tb_books");
            WHERE("tb_books.id=#{id}");
        }}.toString();
    }


    //update book by id
    public String update(int id, Book book) {
        return new SQL() {{
            UPDATE("tb_books");
            SET("title=#{book.title}", "author=#{book.author}",
                    "description=#{book.description}",
                    "category_id=#{book.category.id}");
            WHERE("id=#{id}");
        }}.toString();
    }

    //delete book by id
    public String delete(int id) {
        return new SQL() {{
            DELETE_FROM("tb_books");
            WHERE("id=#{id}");
        }}.toString();
    }

}
