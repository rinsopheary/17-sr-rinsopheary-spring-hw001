package com.hrd.hw01.repository.provider;

import com.hrd.hw01.model.Category;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String select() {
        return new SQL() {{
            SELECT("*");
            FROM("tb_categories");
        }}.toString();
    }

    public String insert() {
        return new SQL() {{
            INSERT_INTO("tb_categories");
            VALUES("title", "#{title}");
        }}.toString();
    }

    public String delete(int id) {
        return new SQL() {{
            DELETE_FROM("tb_categories");
            if (id > 0)
                WHERE("id=#{id}");
        }}.toString();
    }

    public String update(int id, Category category) {
        return new SQL() {{
            UPDATE("tb_categories");
            SET("title=#{category.title}");
            WHERE("id=#{id}");
        }}.toString();
    }

}
