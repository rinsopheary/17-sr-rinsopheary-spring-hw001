package com.hrd.hw01.repository;

import com.hrd.hw01.model.Category;
import com.hrd.hw01.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @InsertProvider(value = CategoryProvider.class, method = "insert")
    boolean insert(Category category);

    @SelectProvider(value = CategoryProvider.class, method = "select")
    List<Category> select();

    @DeleteProvider(value = CategoryProvider.class, method = "delete")
    void delete(int id);

    @UpdateProvider(value = CategoryProvider.class, method = "update")
    boolean update(int id, Category category);

    @Select("select * from tb_categories where id = #{id}")
    Category selectByCategoryId(int id);
}
