package com.hrd.hw01.repository;

import com.hrd.hw01.model.Book;
import com.hrd.hw01.model.Category;
import com.hrd.hw01.repository.provider.BookProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {


    @SelectProvider(value = BookProvider.class, method = "select")
    @Results({
            @Result(
                    property = "category",
                    javaType = Category.class,
                    column = "category_id",
                    one = @One(select = "getCategory")
            ),
    })
    List<Book> select();

    @SelectProvider(value = BookProvider.class, method = "selectBookById")
    @Results({
            @Result(
                    property = "category",
                    javaType = Category.class,
                    column = "category_id",
                    one = @One(select = "getCategory")
            ),
    })
    Book selectBookById(int id);

    @InsertProvider(value = BookProvider.class, method = "insert")
    boolean insert(Book book);

    @DeleteProvider(value = BookProvider.class, method = "delete")
    void delete(int id);

    @UpdateProvider(value = BookProvider.class, method = "update")
    boolean update(int id, Book book);

    @Select("SELECT * FROM tb_categories WHERE id=#{category_id}")
    Category getCategory(int category_id);

}
