package com.hrd.hw01.rest.respone;

import org.springframework.http.HttpStatus;

public class CategoryResponse {
    private String message;
    private HttpStatus status;

    public CategoryResponse() {
    }

    public CategoryResponse(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CategoryResponse{" +
                "message='" + message + '\'' +
                ", status=" + status +
                '}';
    }
}
