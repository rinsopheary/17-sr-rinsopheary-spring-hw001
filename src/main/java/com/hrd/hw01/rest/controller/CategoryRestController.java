package com.hrd.hw01.rest.controller;

import com.hrd.hw01.model.Category;
import com.hrd.hw01.rest.request.CategoryRequestModel;
import com.hrd.hw01.rest.respone.BaseApiResponse;
import com.hrd.hw01.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryRestController {

    public CategoryService service;

    @Autowired
    public void setService(CategoryService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> insert(@RequestBody CategoryRequestModel request) {

        BaseApiResponse<CategoryRequestModel> apiResponse = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        Category category = mapper.map(request, Category.class);

        Category action = service.insert(category);
        CategoryRequestModel result = mapper.map(action, CategoryRequestModel.class);

        apiResponse.setData(result);
        apiResponse.setMessage("Success");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }

    @GetMapping
    public ResponseEntity<BaseApiResponse<List<Category>>> selectAll() {

        BaseApiResponse<List<Category>> apiResponse = new BaseApiResponse<>();

        List<Category> categoryList = service.select();

        apiResponse.setData(categoryList);
        apiResponse.setMessage("Success");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> selectByCategoryId(@PathVariable int id) {

        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();
        Category articleDto;

        articleDto = service.selectByCategoryId(id);

        ModelMapper mapper = new ModelMapper();
        CategoryRequestModel articleRest = mapper.map(articleDto, CategoryRequestModel.class);

        response.setData(articleRest);
        response.setMessage("Success");
        response.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(response);

    }


    @DeleteMapping("/{id}")
    public ResponseEntity<BaseApiResponse<String>> delete(@PathVariable int id) {

        BaseApiResponse<String> apiResponse = new BaseApiResponse<>();

        service.delete(id);

        apiResponse.setMessage("Success");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseApiResponse<String>> update(
            @PathVariable int id, @RequestBody CategoryRequestModel request) {

        BaseApiResponse<String> apiResponse = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        Category category = mapper.map(request, Category.class);

        Category action = service.update(id, category);
        mapper.map(action, CategoryRequestModel.class);

        apiResponse.setMessage("Success");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }

}
