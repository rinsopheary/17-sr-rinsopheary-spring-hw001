package com.hrd.hw01.rest.controller;

import com.hrd.hw01.model.Book;
import com.hrd.hw01.rest.request.BookRequestModel;
import com.hrd.hw01.rest.respone.BaseApiResponse;
import com.hrd.hw01.rest.respone.CategoryResponse;
import com.hrd.hw01.service.BookService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BookRestController {

    BookService service;

    @Autowired
    public BookRestController(BookService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<BaseApiResponse<BookRequestModel>> insert(@RequestBody BookRequestModel request) {
        BaseApiResponse<BookRequestModel> apiResponse = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        Book book = mapper.map(request, Book.class);

        Book action = service.insert(book);
        BookRequestModel result = mapper.map(action, BookRequestModel.class);

        apiResponse.setData(result);
        apiResponse.setMessage("Success");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }

    @GetMapping
    public ResponseEntity<BaseApiResponse<List<Book>>> selectAll() {
        BaseApiResponse<List<Book>> apiResponse = new BaseApiResponse<>();

        List<Book> bookList = service.select();


        apiResponse.setData(bookList);
        apiResponse.setMessage("Success");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseApiResponse<Book>> selectById(@PathVariable int id) {
        BaseApiResponse<Book> apiResponse = new BaseApiResponse<>();

        Book book = service.selectBookByID(id);

        apiResponse.setData(book);
        apiResponse.setMessage("Success");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }


    @PutMapping("/{id}")
    public ResponseEntity<BaseApiResponse<Book>> update(@PathVariable int id, @RequestBody BookRequestModel request) {
        BaseApiResponse<Book> apiResponse = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        Book book = mapper.map(request, Book.class);

        Book action = service.update(id, book);
        mapper.map(action, BookRequestModel.class);

        apiResponse.setData(book);
        apiResponse.setMessage("Success");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<CategoryResponse> delete(@PathVariable int id) {
        service.delete(id);
        CategoryResponse apiResponse = new CategoryResponse();
        apiResponse.setMessage("Success");
        apiResponse.setStatus(HttpStatus.OK);

        return ResponseEntity.ok(apiResponse);
    }
}
