package com.hrd.hw01.service;


import com.hrd.hw01.model.Book;

import java.util.List;

public interface BookService {

    List<Book> select();

    Book selectBookByID(int id);

    Book insert(Book book);

    Book update(int id, Book book);

    void delete(int id);

}
