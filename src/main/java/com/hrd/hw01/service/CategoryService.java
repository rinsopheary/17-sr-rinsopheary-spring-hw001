package com.hrd.hw01.service;


import com.hrd.hw01.model.Category;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public interface CategoryService {

    Category insert(Category category);

    List<Category> select();

    void delete(int id);

    Category update(int id, Category category);

    // Select a category by category id
    Category selectByCategoryId(int articleId) throws ResponseStatusException;
}
