package com.hrd.hw01.service.impl;

import com.hrd.hw01.model.Category;
import com.hrd.hw01.repository.CategoryRepository;
import com.hrd.hw01.rest.respone.Message;
import com.hrd.hw01.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class CategoryImp implements CategoryService {

    private CategoryRepository categoryRepository;

    //inject bean by constructor
    @Autowired
    public CategoryImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public Category insert(Category category) {
        boolean isInserted = categoryRepository.insert(category);
        if(isInserted)
            return category;
        else
            return null;
    }

    @Override
    public List<Category> select() {
        return categoryRepository.select();
    }


    @Override
    public Category update(int id, Category category) {
        boolean isUpdated = categoryRepository.update(id, category);
        if(isUpdated)
            return category;
        else
            return null;
    }

    @Override
    public Category selectByCategoryId(int articleId) throws ResponseStatusException {
        Category articleDto = categoryRepository.selectByCategoryId(articleId);

        if (articleDto != null) {
            return categoryRepository.selectByCategoryId(articleId);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, Message.Error.NO_RECORD_FOUND.value());
        }
    }

    @Override
    public void delete(int id) {
        categoryRepository.delete(id);
    }
}
