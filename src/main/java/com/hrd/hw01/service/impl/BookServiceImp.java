package com.hrd.hw01.service.impl;

import com.hrd.hw01.model.Book;
import com.hrd.hw01.repository.BookRepository;
import com.hrd.hw01.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {

    BookRepository bookRepository;

    @Autowired
    public BookServiceImp(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> select() {
        return bookRepository.select();
    }

    @Override
    public Book selectBookByID(int id) {
        return bookRepository.selectBookById(id);
    }

    @Override
    public Book insert(Book book) {
        boolean isInserted = bookRepository.insert(book);
        if (isInserted)
            return book;
        else
            return null;
    }

    @Override
    public Book update(int id, Book book) {
        boolean isUpdated = bookRepository.update(id, book);
        if (isUpdated)
            return book;
        else
            return null;
    }

    @Override
    public void delete(int id) {
        bookRepository.delete(id);
    }
}
